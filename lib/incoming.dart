import 'dart:async';

import 'package:flutter/services.dart';

class Incoming {
  static const MethodChannel _channel = const MethodChannel('incoming');

  static Future<void> displayCustomIncomingCall(
    String packageName,
    String className, {
    String icon,
    Map<String, dynamic> extra,
    String contentTitle,
    String answerText,
    String declineText,
    String ringtoneUri,
  }) async {
    assert(packageName != null);
    assert(className != null);
    await _channel.invokeMethod('displayCustomIncomingCall', {
      'packageName': packageName,
      'className': className,
      'icon': icon,
      'extra': extra ?? Map(),
      'contentTitle': contentTitle ?? 'Incoming call',
      'answerText': answerText ?? 'Answer',
      'declineText': declineText ?? 'Decline',
      'ringtoneUri': ringtoneUri,
    });
  }

  static Future<void> dismissCustomIncomingCall() async {
    await _channel.invokeMethod('dismissCustomIncomingCall');
  }
}
