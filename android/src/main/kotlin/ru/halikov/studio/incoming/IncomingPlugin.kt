package ru.halikov.studio.incoming

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import androidx.annotation.NonNull
import androidx.core.app.NotificationCompat
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar


/** IncomingPlugin */
public class IncomingPlugin : FlutterPlugin {


    companion object {
        @JvmStatic
        fun registerWith(registrar: Registrar) {
            val channel = MethodChannel(registrar.messenger(), "incoming")
            val plugin = IncomingHandler(channel, registrar.context().applicationContext)
            channel.setMethodCallHandler(plugin)

        }
    }

    private var methodCallHandler: IncomingHandler? = null

    override fun onAttachedToEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        val channel = MethodChannel(binding.flutterEngine.dartExecutor, "incoming")
        val plugin = IncomingHandler(channel, binding.applicationContext)

        methodCallHandler = plugin
    }

    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        methodCallHandler = null
    }
}


class IncomingHandler(private val channel: MethodChannel, private var applicationContext: Context) : MethodChannel.MethodCallHandler {
    private val NOTIFICATION_ID = 38496

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        when (call.method) {
            "displayCustomIncomingCall" -> {
                displayCustomIncomingCall(call.argument("packageName")!!, call.argument("className")!!, call.argument("icon"), call.argument("extra")!!, call.argument("contentTitle")!!, call.argument("answerText")!!, call.argument("declineText")!!, call.argument("ringtoneUri"))
                result.success(null)
            }
            "dismissCustomIncomingCall" -> {
                dismissCustomIncomingCall()
                result.success(null)
            }
            else -> {
                result.notImplemented()
            }
        }
    }

    private fun displayCustomIncomingCall(packageName: String, className: String, icon: String?, extra: HashMap<String, String>, contentTitle: String, answerText: String, declineText: String, ringtoneUri: String?) {
        val fullScreenIntent = Intent()
        fullScreenIntent.setClassName(packageName, "$packageName.$className")
        val fullScreenPendingIntent = PendingIntent.getActivity(applicationContext, 0,
                fullScreenIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notificationBuilder =
                NotificationCompat.Builder(applicationContext, "incoming_calls")
                        .setContentTitle("Incoming call")
                        .setContentText("(919) 555-1234")
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setCategory(NotificationCompat.CATEGORY_CALL)

                        // Use a full-screen intent only for the highest-priority alerts where you
                        // have an associated activity that you would like to launch after the user
                        // interacts with the notification. Also, if your app targets Android 10
                        // or higher, you need to request the USE_FULL_SCREEN_INTENT permission in
                        // order for the platform to invoke this notification.
                        .setFullScreenIntent(fullScreenPendingIntent, true)
        if (icon != null && icon.isNotEmpty()) {
            notificationBuilder.setSmallIcon(applicationContext.resources.getIdentifier(icon, "drawable", applicationContext.packageName))
        } else {
            notificationBuilder.setSmallIcon(android.R.drawable.stat_sys_phone_call);
        }
        val incomingCallNotification = notificationBuilder.build()




        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


        notificationManager.notify(NOTIFICATION_ID, incomingCallNotification)
    }

    private fun dismissCustomIncomingCall() {
        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(NOTIFICATION_ID)
    }
}