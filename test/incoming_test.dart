import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:incoming/incoming.dart';

void main() {
  const MethodChannel channel = MethodChannel('incoming');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await Incoming.platformVersion, '42');
  });
}
